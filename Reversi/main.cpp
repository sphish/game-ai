#include <bits/stdc++.h>
#include "jsoncpp/json.h" // C++编译时默认包含此库

using namespace std;

typedef int Piece;
typedef bool Player;
typedef pair<int, int> Coordinate;
const int gridWidth = 8;
const Player black = true, white = false;
const int Threshold = 64;
const int valueOfPos[gridWidth][gridWidth] = {
	30, 2, 20, 15, 15, 20,  2, 30,
	2,  1,  2,  8,  8,  2,  1,  2,
	20, 2,  9,  9,  9,  9,  2, 20,
	15, 8,  9,  2,  2,  9,  8, 15,
	15, 8,  9,  2,  2,  9,  8, 15,
	20, 2,  9,  9,  9,  9,  2, 20,
	2,  1,  2,  8,  8,  2,  1,  2,
	30, 2, 20, 15, 15, 20,  2, 20
};
const clock_t startTime = clock();
const clock_t TIMELIMIT = 0.9 * CLOCKS_PER_SEC;

Piece board[8][8] = { 0 }; // 先x后y，记录棋盘状态
int blackPieceCount = 2, whitePieceCount = 2;
Player currentPlayer;
int pieceCount[2];

struct SearchNode {
	Piece board[gridWidth][gridWidth];
	int pieceCount[2];
	Player currentPlayer;

	SearchNode *father;
	map<Coordinate, SearchNode*> child;
	double evaluation; int visitCount;

	SearchNode() {
		memset(board, 0, sizeof board);
		currentPlayer = 1;
		pieceCount[0] = pieceCount[1] = 0;
		father = NULL;
		visitCount = 0;
		evaluation = 0.;
	}

	double get_value() const {
		return visitCount ? 
			evaluation / visitCount : -100.;
	}
};

// back up the current game states.
void backup(SearchNode &x) {
	memcpy(x.board, board, sizeof board);
	x.currentPlayer = currentPlayer;
	x.pieceCount[white] = pieceCount[white];
	x.pieceCount[black] = pieceCount[black];
}

// recover game states from a backup of the origin game.
void recover(const SearchNode &x) {
	memcpy(board, x.board, sizeof board);
	currentPlayer = x.currentPlayer;
	pieceCount[white] = x.pieceCount[white];
	pieceCount[black] = x.pieceCount[black];
}

// step according the given direction
bool step_directly(int &x, int &y, int *direction) {
	x += direction[0];
	y += direction[1];
	return (x < 0 || y < 0 || x >= gridWidth || y >= gridWidth);
}

// determine whether a piece can form a siege by given position and direction.
bool besiege(Coordinate pos, Player player, int *direction) {
	if (!direction[0] && !direction[1])
		return false;

	int x = pos.first, y = pos.second;
	if (board[x][y] != -1)
		return false;
	
	if (step_directly(x, y, direction)) 
		return false;

	if (board[x][y] != (player ^ 1))
		 return false;

	while (!step_directly(x, y, direction)) {
		if (board[x][y] == player)
			return true;
		if (board[x][y] == -1)
			return false;
	}	

	return false;
}

// determine whether a position could be placed a piece by given player.
bool placeable(Coordinate pos, Player player) {
	int x = pos.first, y = pos.second;
	if (board[x][y] != -1)
		return false;

	for (int i = -1; i <= 1; i++)
		for (int j = -1; j <= 1; j++) {
			int direction[2] = {i, j};
			if (besiege(pos, player, direction))
				return true;
		}

	return false;	
}  

// determine whether the game reaches the terminal state
bool terminal() {
	if (pieceCount[white] + pieceCount[black] >= gridWidth * gridWidth)
		return true;

	for (int i = 0; i < gridWidth; i++)
		for (int j = 0; j < gridWidth; j++)
			if (placeable(make_pair(i, j), white) 
				|| placeable(make_pair(i, j), black))
				return false;

	return true; 
}

// determine whether the player is the winner
double is_winner(Player player) {
	int delta = pieceCount[player] - pieceCount[player ^ 1];
	if (delta > 20)
		return 1.;
	if (delta > 5)
		return 0.7;
	if (delta > 0)
		return 0.55;
	if (delta < -20)
		return 0;
	if (delta < -5)
		return 0.3;
	if (delta < 0)
		return 0.45;
	return 0.5;
}

// use UCT to get the evalution of search node
double search_value(const SearchNode* x, const SearchNode *child) {
	if (child->visitCount == 0) return -100;
	return child->get_value() 
		+ 2 * sqrt(2 * log(x->visitCount) / child->visitCount);
}

// find a search node to expand
pair<SearchNode*, bool> select_node(SearchNode* x) {
	if (terminal()) return make_pair(x, false);
	if (x->visitCount < Threshold) 
		return make_pair(x, false);

	SearchNode* y = x;
	double minValue = -200.;
	for (auto e : x->child) {
		double temp = search_value(x, e.second);
		if (temp > minValue) {
			y = e.second;
			minValue = temp;
		}
	}
	recover(*y);

	return make_pair(y, y != x);
}

// place a piece by given player
void place_piece(Coordinate pos, Player player) {
	if (pos == make_pair(-1, -1))
		return ;
	
	int x = pos.first, y = pos.second;

	for (int i = -1; i <= 1; i++)
		for (int j = -1; j <= 1; j++) {
			int direction[2] = {i, j};
			if (!(i | j)) 
				continue ;
			if (besiege(pos, player, direction)) {
				int _x = x + i, _y = y + j;
				while (board[_x][_y] != player) {
					board[_x][_y] = player;
					_x += i; _y += j;
					pieceCount[player]++;
					pieceCount[player ^ 1]--;
				}
			}
		}

	pieceCount[board[x][y] = player]++;	
}

SearchNode* expand(SearchNode *x) {
	if (terminal()) return x;
	map<Coordinate, int> alterList;

	Coordinate choice;
	int sum = 0;
	for (int i = 0; i < gridWidth; i++)
		for (int j = 0; j < gridWidth; j++)
			if (placeable(make_pair(i, j), currentPlayer)) {
				alterList[make_pair(i, j)] = valueOfPos[i][j];
				sum += valueOfPos[i][j];
			}

	// randomly choose a place with value as their weight.
	if (!sum)
		choice = make_pair(-1, -1);
	else {
		int temp = rand() % sum;
		for (auto e : alterList) {
			if ((temp -= e.second) < 0) {
				choice = e.first;
				break ;
			}
		}
	}

	auto it = x->child.find(choice);
	if (it != x->child.end()) {
		recover(*(it->second));
		return it->second;
	}

	SearchNode *y = new SearchNode();
	place_piece(choice, currentPlayer);
	currentPlayer ^= 1;
	y->father = x;
	backup(*y);
	x->child[choice] = y; 
	return y;		
}

void simulate(const SearchNode *x, double rank[2]) {
	Coordinate choice;

	while (!terminal()) {
		map<Coordinate, int> alterList;
		int sum = 0;

		for (int i = 0; i < gridWidth; i++)
			for (int j = 0; j < gridWidth; j++) 
				if (placeable(make_pair(i, j), currentPlayer)) {
					alterList[make_pair(i, j)] = valueOfPos[i][j];
					sum += valueOfPos[i][j];
				}
		
		if (sum) {
			int temp = rand() % sum;
			for (auto e : alterList) {
				if ((temp -= e.second) < 0) {
					choice = e.first;
					break ;
				}
			}
		} else 
			choice = make_pair(-1, -1);
		place_piece(choice, currentPlayer);
		currentPlayer ^= 1;
	}
	rank[white] = is_winner(white);
	rank[black] = is_winner(black);
	recover(*x);
}

void back_propagate(SearchNode *x, double rank[2]) {
	while (x != NULL) {
		x->visitCount++;
		x->evaluation += rank[x->currentPlayer ^ 1];
		x = x->father;
	}
}

void monte_carlo(int &resultX, int &resultY) {
	SearchNode *rootNode, *currentNode;
	rootNode = new SearchNode();
	pair<SearchNode*, bool> result;
	double r[2];
	backup(*rootNode);
	
	while (clock() - startTime < TIMELIMIT) {
		currentNode = rootNode;
		do {
			result = select_node(currentNode);
			currentNode = result.first;
		} while(result.second);

		currentNode = expand(currentNode);
		simulate(currentNode, r);
		back_propagate(currentNode, r);
		recover(*rootNode);
	}

	double bestValue = -100;
	Coordinate choice = make_pair(-1, -1);

	for (auto e : rootNode->child) {
		double temp = e.second->get_value();
		if (temp > bestValue) {
			choice = e.first;
			bestValue = temp;
		}
	}
	resultX = choice.first;
	resultY = choice.second;
}

int main()
{
	int x, y;

	// 初始化棋盘
	memset(board, -1, sizeof board);
	board[3][4] = board[4][3] = 1;  //|白|黑|
	board[3][3] = board[4][4] = 0; //|黑|白|
	pieceCount[white] = pieceCount[black] = 2;

	// 读入JSON
	string str;
	getline(cin, str);
	Json::Reader reader;
	Json::Value input;
	reader.parse(str, input);

	// 分析自己收到的输入和自己过往的输出，并恢复状态
	int turnID = input["responses"].size();
	currentPlayer = input["requests"][(Json::Value::UInt) 0]["x"].asInt() < 0 ? 
		black : white; // 第一回合收到坐标是-1, -1，说明我是黑方
	for (int i = 0; i < turnID; i++)
	{
		// 根据这些输入输出逐渐恢复状态到当前回合
		x = input["requests"][i]["x"].asInt();
		y = input["requests"][i]["y"].asInt();
		if (x >= 0)
			place_piece(make_pair(x, y), currentPlayer ^ 1); // 模拟对方落子
		x = input["responses"][i]["x"].asInt();
		y = input["responses"][i]["y"].asInt();
		if (x >= 0)
			place_piece(make_pair(x, y), currentPlayer); // 模拟己方落子
	}

	// 看看自己本回合输入
	x = input["requests"][turnID]["x"].asInt();
	y = input["requests"][turnID]["y"].asInt();
	if (x >= 0)
		place_piece(make_pair(x, y), currentPlayer ^ 1); // 模拟对方落子

	// 做出决策
	int resultX, resultY;
	monte_carlo(resultX, resultY);

	Json::Value ret;
	ret["response"]["x"] = resultX;
	ret["response"]["y"] = resultY;
	Json::FastWriter writer;
	cout << writer.write(ret) << endl;
	return 0;
}