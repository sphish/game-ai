#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <vector>
#include <cstring>
#include <cstdlib>
#include "jsoncpp/json.h"
using namespace std;

typedef unsigned long long group;

const int width = 8;

vector<int> nbor[width*width];
int resultX, resultY;

int stones = 10000;
int liber = 2;
int movement = 3;
int occupy = 3;
int camp = -10;
int stables = 8;
//const float sideff[6] = {0, 0.02, 0.05, 0.15, 0.4};
//const float urgency[5] = {0, 0.01, 0.03, 0.08, 0.2};
int layer, turnID, xx, yy;
clock_t start = clock();
clock_t threshold = 0.95 * CLOCKS_PER_SEC;

class Board {
public:
	static int campOrder, incamp[64], liberty[64], neiCol[64], neiCamp[64], liberCamp[64];
	static vector<int> camps[64], stepx, stepy;
	static vector<Board> history;
	static Board sb[width][width];

	group player[2];
	int current;

	static int xy_to_l(int x, int y) {
		return x * width + y;
	}

	static bool have(group x, int p) {
		return (x >> p) & 1;
	}

	static void rev(group &x, int p) {
		x ^= 1ll  << p;
	}

	static void initialize() {
		for (int i = 0; i < width; i++)
			for (int j = 0; j < width; j++) {
				int p = xy_to_l(i, j);
				if (j > 0) nbor[p].push_back(p - 1);
				if (j < width - 1) nbor[p].push_back(p + 1);
				if (i > 0) nbor[p].push_back(p - width);
				if (i < width - 1) nbor[p].push_back(p + width);
			}
	}

	int status(int p) {
		if (have(player[0], p)) return 0;
		if (have(player[1], p)) return 1;
		return -1;
	}

	Board(group a = 0, group b = 0, int c = 0) {
		player[0] = a; player[1] = b;
		current = c;
	}

	void findComrade(int p) {
		camps[campOrder].push_back(p);
		incamp[p] = campOrder;
		int stp = status(p);

		for (auto np : nbor[p]) {
			int stnp = status(np);
			if (stnp == stp && incamp[np] == -1)
				findComrade(np);
			else if (stnp == -1) liberty[campOrder]++;
		}
	}

	void preprocess() {
		memset(incamp, -1, sizeof incamp);
		memset(neiCamp, -1, sizeof neiCol);
		memset(liberCamp, 0, sizeof liberCamp);
		campOrder = 0;

		for (int i = 0; i < 64; i++) {
			int c = status(i);
			if (incamp[i] == -1) {
				liberty[campOrder] = 0;
				camps[campOrder].clear();
				findComrade(i);
				campOrder++;
			}
		}
	}
	static int stable(int x) {
		int c = incamp[x];
		if (neiCamp[c] >= 0)
			if (liberCamp[neiCamp[c]] > 1)
				return neiCol[c];
		return -1;
	}
	void takeaway(int p, int color) {
		for (auto x : camps[incamp[p]])
			rev(player[color], x);
	}

	Board settle(int x, int y, int color) {
		int p = xy_to_l(x, y);
		Board nb = *this; 
		nb.current = current ^ 1;
		if (x == -1) return nb;
		
		if (status(p) != -1) {
			return Board(-1, -1, -1);
		}

		
		rev(nb.player[color], p);

		// if be able to take away neighborhood stones.
		int liber = nbor[p].size();
		for (auto np : nbor[p]) 
			if (status(np) != -1 && incamp[np] != -1)
				liberty[incamp[np]]--, liber--;

		int ok = 0;
		for (auto np : nbor[p])
			if (status(np) != -1 && incamp[np] != -1 && liberty[incamp[np]] == 0) {
				if (nb.status(np) == (color ^ 1)) {
					nb.takeaway(np, color ^ 1);
					ok++;
				}
			}

		if (!ok && !liber) {	
			for (auto np : nbor[p])
				if (status(np) != -1 && incamp[np] != -1 && liberty[incamp[np]] != 0) {
					if (nb.status(np) == color) {
						ok = true;
					}
				}

			if (!ok) {
				for (auto np : nbor[p])
					if (status(np) != -1 && incamp[np] != -1)
						liberty[incamp[np]]++;
				return Board(-1, -1, -1);
			}
		}

		for (auto np : nbor[p])
			if (incamp[np] != -1)
				liberty[incamp[np]]++;

		return nb;
	}

	void getAllStep() {
		stepx.clear();
		stepy.clear();

		for (int i = 0; i < width; i++)
			for (int j = 0; j < width; j++) {
				sb[i][j] = settle(i, j, current);
				if (sb[i][j].current != -1) {
					bool ok = true;
					for (auto h : history) 
						if (sb[i][j].player[0] == h.player[0]
							&& sb[i][j].player[1] == h.player[1]) {
								ok = false;
								break;
							}
					if (ok) {
						stepx.push_back(i);
						stepy.push_back(j);
					}
				}
			}
	}

	pair<int, int> explore(int x, group &vis) {
		int count = 1, wb = 0;
		rev(vis, x);
		for (auto np : nbor[x]) {
			int st = status(np);
			if (st == -1 && !have(vis, np)) {
				pair<int, int> tmp = explore(np, vis);
				count += tmp.first;
				wb |= tmp.second;
			}
			else if (st >= 0) {
				int camp = incamp[x];
				wb |= 1 << st;
				if (neiCamp[camp] == -1)
					neiCamp[camp] = incamp[np];
				else if (neiCamp[camp] != incamp[np])
					neiCamp[camp] = -2;
			}
		}
		return make_pair(count, wb);
	}

	float getScore() {
		float ret = 0;
		group vis = 0;
		for (int i = 0; i < width * width; i++) 
			if (status(i) == -1 && !have(vis, i)) {
				auto tmp = explore(i, vis);
				if (tmp.second == 3) {
					ret += 0.5 * tmp.first;
					neiCol[incamp[i]] = -1;
				} else if (tmp.second == (1 << current))
					ret += tmp.first, neiCol[incamp[i]] = tmp.second >> 1;
				if (neiCamp[incamp[i]] >= 0)
					liberCamp[neiCamp[incamp[i]]]++;
			}
			else if (status(i) == current)
				ret += 1;

		return ret;
	}

	float getValue() {
		preprocess();
		group count = 0;
		float result = 0;
		for (int i = 0; i < width * width; i++) {
			int p = status(i);
			if (p == current) {
				result += stones;
				if (!have(count, incamp[i])) {
					rev(count, incamp[i]);
					result += camp;
					result += liber * liberty[incamp[i]];
				}
			} else if (p == (current ^ 1)) {
				result -= stones;
				if (!have(count, incamp[i])) {
					rev(count, incamp[i]);
					result -= camp;
					result -= liber * liberty[incamp[i]];
				}
			} else {
				if (stable(i) != -1)
					result += (stable(i) == current ? 1 : -1) * stables;	
			}
		}
		result += occupy * (1 + ((float)turnID / 30.) * ((float)turnID / 30.)) * getScore();
		return result;
	}

	int winner() {
		float t[2] = {-0.75, 0.75};
		preprocess();
		float s = getScore() + t[current];
		return current ^ (s < 32.);
	}
	
	void graph() {
		puts("---------------------------");
		for (int j = 0; j < 8; j++) {
			for (int i = 0; i < 8; i++) {
				int p = status(xy_to_l(i, j));
				if (p == -1) printf(" |");
				if (p == 1) printf("O|");
				if (p == 0) printf("X|");
			}
			puts("");
			puts("----------------");
		}
		puts("---------------------------");
	}
} board;

int Board::campOrder = 0;
int Board::incamp[64], Board::liberty[64];

vector<int> Board::camps[64], Board::stepx, Board::stepy; 
int Board::neiCamp[64];
int Board::neiCol[64], Board::liberCamp[64];
vector<Board> Board::history;
Board Board::sb[width][width];
vector<Board> ch;

struct SearchNode {
	Board board;
	int sum, mm;
	SearchNode *father;
	map<int, Board> sb;
	map<int, int> qv;
	map<int, SearchNode*> child;
	double evaluation; int visitCount;


	double get_value() const {
		return visitCount ?
			evaluation / visitCount : -100.;
	}

	bool terminal() {
		return random_move().first == -1; 
	}
	void get_move() {
		board.preprocess();
		board.getScore();
		board.getAllStep();
		
		sum = 0;
		mm = 1e9;
		sb.clear();
		qv.clear();
		for (int i = 0; i < Board::stepx.size(); i++) {
			int p = Board::stepx[i] * width + Board::stepy[i];
			if (Board::stable(p) == -1 && Board::neiCol[Board::incamp[p]] != board.current) 
				sb[p] = Board::sb[Board::stepx[i]][Board::stepy[i]];
		}

		for (auto e : sb) {
			qv[e.first] = -e.second.getValue();
			mm = min(qv[e.first], mm);
		}

		for (auto e : sb) {
			qv[e.first] += -mm + 1;
			sum += qv[e.first];
		}
	}

	pair<int, Board> random_move() {
		Board nb = board; nb.current ^= 1;
		if (sb.size() == 0) {
			return make_pair(-1, nb);	
		}
		
		int temp = rand() % sum;
		for (auto e : sb) {
			if ((temp -= qv[e.first]) < 0) {		
				for (auto g : ch)
					if (e.second.player[0] == g.player[0] && e.second.player[1] == g.player[1]) 
						return make_pair(-1, nb);
				return e;
			}
		}


		
	}

	SearchNode(Board b) {
		father = NULL;
		visitCount = 0;
		evaluation = 0.;
		sum = 0;
		board = b;
		get_move();
	}

};


// use UCT to get the evalution of search SearchNode
double search_value(const SearchNode* x, const SearchNode *child) {
	if (child->visitCount == 0) return -100;
	return child->get_value() 
		+ 2 * sqrt(2 * log(x->visitCount) / child->visitCount);
}

// find a search node to expand
pair<SearchNode*, bool> select_node(SearchNode* x) {
	if (x->terminal()) return make_pair(x, false);
	if (x->visitCount < 16) 
		return make_pair(x, false);

	SearchNode* y = x;
	double minValue = -200.;
	for (auto e : x->child) {
		double temp = search_value(x, e.second);
		if (temp > minValue) {
			y = e.second;
			minValue = temp;
		}
	}

	return make_pair(y, y != x);
}

SearchNode* expand(SearchNode *x) {
	if (x->terminal()) return x;

	
	pair<int, Board> tmp = x->random_move();

	auto it = x->child.find(tmp.first);
	if (it != x->child.end()) {
		return it->second;
	}

	SearchNode *y = new SearchNode(tmp.second);
	y->father = x;
	x->child[tmp.first] = y; 
	return y;		
}

void simulate(const SearchNode *o, double rank[2]) {
	SearchNode x = *o;
	ch.clear();
	int cnt = 0;
	while (!x.terminal()) {
		ch.push_back(x.board);
		auto rm = x.random_move();
		x = rm.second;
		x.get_move();
	}
	int winner = x.board.winner();
	rank[winner] = 1; rank[winner ^ 1] = 0;
}

void back_propagate(SearchNode *x, double rank[2]) {
	while (x != NULL) {
		x->visitCount++;
		x->evaluation += rank[x->board.current ^ 1];
		x = x->father;
	}
}

void monte_carlo(int &resultX, int &resultY) {
	SearchNode *rootNode, *currentNode;
	rootNode = new SearchNode(board);
	pair<SearchNode*, bool> result;
	double r[2];
	int cnt = 0;	
	while (clock() - start < threshold) {
		currentNode = rootNode;
		do {
			result = select_node(currentNode);
			currentNode = result.first;
		} while(result.second);

		currentNode = expand(currentNode);
		simulate(currentNode, r);
		back_propagate(currentNode, r);
	}

	double bestValue = -100;
	int p = -1;

	for (auto e : rootNode->child) {
		double temp = e.second->get_value();
		if (e.first == -1) {
			if (rootNode->child.size() > 1)
				continue;
		}
		if (temp > bestValue) {
			p = e.first;
			bestValue = temp;
		}
	}
	resultX = p / width;
	resultY = p % width;
}

float absearch(Board board, float alpha, float beta, int lay) {
	if (lay == layer) {
		return board.getValue();
	}

	float result = -1e9;
	board.preprocess();
	board.getScore();
	board.getAllStep();

	map<int, Board> sb;
	for (int i = 0; i < Board::stepx.size(); i++) {
		int p = Board::stepx[i] * width + Board::stepy[i];
		if (Board::stable(p) == -1 && Board::neiCol[Board::incamp[p]] != board.current) 
			sb[p] = Board::sb[Board::stepx[i]][Board::stepy[i]];
	}
	if (sb.size() == 0) {
		Board nb = board; nb.current ^= 1;
		sb[-1] = nb;
	}

	int resultC = 1;
	for (auto xb : sb) {
		float ret = -absearch(xb.second, -beta, -max(alpha, result), lay + 1);

		if (clock() - start > threshold)
			return result;
		if (lay == 0 && ret == result && rand() % resultC++ == 0)
			resultX = xb.first / 8, 
			resultY = xb.first % 8, 
			resultC = 1;
		if (ret > result) {
			result = ret;
			if (lay == 0)
				resultX = xb.first / 8, 
				resultY = xb.first % 8, 
				resultC = 1;
			if (result > beta)
				return result;
		}
	}
	return result;
}

int main(int argc, char* argv[]) {
	Board::initialize();

	srand(time(NULL));
	int currentPlayer;
	// 读入JSON
	string str;
	getline(cin, str);
	Json::Reader reader;
	Json::Value input;
	reader.parse(str, input);

	// 分析自己收到的输入和自己过往的输出，并恢复状态
	turnID = input["responses"].size();
	currentPlayer = input["requests"][(Json::Value::UInt) 0]["x"].asInt() < 0 ? 
		0 : 1; // 第一回合收到坐标是-1, -1，说明我是黑方
	
	int x,  y;
	for (int i = 0; i < turnID; i++)
	{
		#ifdef DEBUG
		printf("TurnId: %d\n", i);
		#endif
		// 根据这些输入输出逐渐恢复状态到当前回合
		x = input["requests"][i]["x"].asInt();
		y = input["requests"][i]["y"].asInt();
		if (x >= 0) {
			Board::history.push_back(board);
			board.preprocess();
			board = board.settle(x - 1, y - 1, currentPlayer ^ 1); // 模拟对方落子
		}
		x = input["responses"][i]["x"].asInt();
		y = input["responses"][i]["y"].asInt();
		if (x >= 0) {
			Board::history.push_back(board);
			board.preprocess();
			board = board.settle(x - 1, y - 1, currentPlayer);
		}
	}

	// 看看自己本回合输入
	xx = x = input["requests"][turnID]["x"].asInt();
	yy = y = input["requests"][turnID]["y"].asInt();
	if (x >= 0) {
		Board::history.push_back(board);
		board.preprocess();
		board = board.settle(x - 1, y - 1, currentPlayer ^ 1); // 模拟对方落子
	}

	// 做出决策
	board.current = currentPlayer;
	int resX = -1, resY = -1;

	if (turnID < -10) {
		monte_carlo(resultX, resultY);
		resX = resultX, resY = resultY;
		if (resY == -1) resX = -1;
		else resX++, resY++;
	} else {
		while (++layer) {
			absearch(board, -1e9, 1e9, 0);
			if (clock() - start > threshold)
				break ;
			resX = resultX, resY = resultY;
			if (resY == -1) resX = -1;
			else resX++, resY++;
		}
	}

	int stars[2][4] = {{3, 6, 3, 6}, {3, 6, 6, 3}};
	if (turnID <= 4) {
		for (int i = 0; i < 4; i++)
			if (board.status(stars[0][i] * 8 + stars[1][i] - 9) == -1)
				resX = stars[0][i], resY = stars[1][i];
	}
	Json::Value ret;
	ret["response"]["x"] = resX;
	ret["response"]["y"] = resY;
	#ifdef LOCAL
	ret["score"] = board.getScore();
	#endif
	Json::FastWriter writer;
	cout << writer.write(ret) << endl;
	#ifdef LOCAL
	board.graph();
	#endif
	return 0;
}