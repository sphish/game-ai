from playground import play
from copy import deepcopy
from random import randint
from threading import Thread
import math

FRAGMENT_LEN = 8
MUTATION = 0.03
PARACOUNT = 6
POPULATION = 16

class PlayThread(Thread):
	def __init__(self, player, number):
		Thread.__init__(self)
		self.player = player
		self.number = number
	
	def run(self):
		self.result = play(self.player, str(self.number))

def decode(x):
	return (x - (1 << (FRAGMENT_LEN - 1))) / (1 << FRAGMENT_LEN - 1)

def translate(DNA):
	ret = []
	mask = (1 << FRAGMENT_LEN) - 1
	for i in range(PARACOUNT):
		ret.append(decode(DNA & mask))
		DNA >>= FRAGMENT_LEN;
	return ret

def hybirdise(parent):
	ret = 0
	for i in range(PARACOUNT * FRAGMENT_LEN):
		ret = (ret << 1) | (parent[randint(0, 1)] & 1)
		if randint(0, 99) < MUTATION * 100:
			ret ^= 1
		parent[0] >>= 1
		parent[1] >>= 1
	
	return ret


def iterate(parent):
	children = deepcopy(parent)
	for i in range(POPULATION - 2):
		children.append(hybirdise(deepcopy(parent)))

	def form(s):
		return s[1:].strip(']').replace(',', '')

	THREAD = 8;
	score = [0 for i in range(POPULATION)]
	conts = []
	for i in range(0, POPULATION):
		for j in range(0, i):
			conts.append([i, j])

	for p in range(len(conts) // THREAD):
		threads = []
		for t in range(THREAD):
			i = conts[p * THREAD + t][0]
			j = conts[p * THREAD + t][1]
			print('player' + str(i) + ' VS player' + str(j) + ' on playground', t)
			player = ['./pg' + str(t) + ' ' + form(str(translate(children[i]))),
				'./pg' + str(t) + ' ' + form(str(translate(children[j])))]
			threads.append(PlayThread(player, t))

		for t in threads:
			t.start()
		for t in threads:
			t.join()
		for t in range(THREAD):
			i = conts[p * THREAD + t][0]
			j = conts[p * THREAD + t][1]
			if threads[t].result == 0:
				score[i] += 1
				print('player' + str(i) + ' win on playground', str(t))
			else:
				score[j] += 1
				print('player' + str(j) + ' win on playground', str(t))

	log = open("log", "w+")

	print("score:", score)
	print("score:", score, file = log)
	oldParent = deepcopy(parent)
	maxS = max(score)
	for i in range(len(score)):
		if (score[i] == maxS):
			parent[0] = deepcopy(children[i])
			score[i] = -1
			break
	
	maxS = max(score)
	for i in range(len(score)):
		if (score[i] == maxS):
			parent[1] = deepcopy(children[i])
			score[i] = -1
			break

	delta = 0
	a = translate(oldParent[0])
	b = translate(parent[0])

	print("Iterated, the winner's param is", b)
	print("Iterated, the winner's param is", b, file = log)
	f = open("param", "w")
	print(parent, file = f)
	print(b, file = f)
	print(translate(parent[1]), file = f)
	f.close()

	for i in range(PARACOUNT):
		delta += (math.fabs(a[i] - b[i]) / (a[i] + 0.00000001)) ** 2
	print("delta = ", delta)
	print("delta = ", delta, file = log)
	log.close()
	return delta
	 
parent = [212762464704, 32942952904]
log = open("log", "w")
log.close()
while True:
	delta = iterate(parent)

