import json
import os

# runplay = ["./debug", "./debug"] # 运行两个选手程序的命令

def play(runplay, ident = ""):
	opts = [[], []]
	subsidy = [-0.75, 0.75]
	passop = {"x":-1, "y":-1}

	def generateInput(p):
		inp = {"requests":[], "responses":[]}	
		if p == 0:
			inp["requests"].append({"x":-2, "y":-2})
		
		for op in opts[player ^ 1]:
			inp["requests"].append(op)
		for op in opts[player]:
			inp["responses"].append(op)

		return inp
	player = 0

	while (True):
		inp = open("in" + ident + ".json", "w")
		print(json.dumps(generateInput(player)), file = inp)
		inp.close()
		os.system(runplay[player] + " < in" + ident + ".json > out" + ident + ".json")
		out = open("out" + ident + ".json", "r")
		output = out.readline()
		#print(output)
		#for line in out:
		#	print(line)
		js = json.loads(output)
		out.close()
		respones = js["response"]
		score = float(js["score"])
		if respones == passop and len(opts[player]) > 0 and opts[player][-1] == passop:
			if score + subsidy[player] > 32:
				# print("player " + str(player) + " win!")
				return player
			else: 
				# print("player " + str(player ^ 1) + " win!")
				return player ^ 1
			break 
		opts[player].append(respones)
		player ^= 1



