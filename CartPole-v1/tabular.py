import numpy as np
import gym
from math import radians

STATE = 270
state = np.zeros(STATE)
Q = np.zeros(STATE)
V = np.zeros(STATE)
W = np.zeros(STATE)
E = np.zeros(STATE)
e = np.zeros(STATE)

def critic(reward_, q_):
	learn = 0.5
	decay = 0.8
	gamma = 0.95

	global V, E, state
	q = 0 if reward_ == -1 else V.dot(state)

	reward = reward_ + gamma * q - q_
	V += learn * reward * E
	E = E * decay + (1 - decay) * state

	return reward, q

def action(reward): 
	learn = 1000
	decay = 0.9
	epsilon = 0.01

	global W, e, state
	act = 1 if W.dot(state) + epsilon * np.random.randn() >= 0 else 0
	W += learn * reward * e
	e = decay * e + (1 - decay) * (act * 2 - 1) * state

	return act

def tabular(observation):
	pos, cartV, theta, poleV = observation 
	# cart position, cart velocity, pole angle and pole velocity at tip.

	if pos < -0.8: 
		a = 0 
	elif pos < 0.8: 
		a = 1
	else:
		a = 2

	if cartV < -0.5:
		b = 0
	elif cartV < 0.5:
		b = 1
	else:
		b = 2
	
	if theta < -1 * radians(6):
		c = 0
	elif theta < -1 * radians(1):
		c = 1
	elif theta < 0:
		c = 2
	elif theta < radians(1):
		c = 3
	elif theta < radians(6):
		c = 4
	else:
		c = 5

	if poleV < -0.9:
		d = 0
	elif poleV < -0.3:
		d = 1
	elif poleV < 0.3:
		d = 2
	elif poleV < 0.9:
		d = 3
	else:
		d = 4

	res = np.zeros(STATE)
	res[a + b * 3 + c * 9 + d * 54] = 1
	return res
		
EPISODES = 10000
STEPS = 100000
env = gym.make('CartPole-v1')
stepCount = 0
for episode in range(EPISODES):
	observation = env.reset()
	p = 0

	for step in range(STEPS):
		stepCount += 1
		state = tabular(observation)
		reward, p = critic(0, p)
		observation, _, done, _ = env.step(action(reward))

		# env.render()
		if done:
			if (step < 499):
				state = tabular(observation)	
				reward, p = critic(-1, p)
				action(reward)
			break

	if episode % 100 == 99:
		print("After {0:5} episodes, the last 100 episodes' average step is {1:5}".format(episode + 1, stepCount / 100))
		stepCount = 0


