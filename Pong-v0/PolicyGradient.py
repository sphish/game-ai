# environment:
# python 3.6.4
# pytorch 0.4.0
# cuda 9.1
import os
import gym
from itertools import count
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Categorical

def preprocess(I):
	# preprocess 210x160x3 picture into 6400
	I = I[35:195]
	I = I[::2, ::2, 0]
	I[I == 144] = 0
	I[I == 109] = 0
	I[I != 0 ] = 1
	return I.astype(np.float).ravel()

class PolicyNet(nn.Module):
	def __init__(self):
		super(PolicyNet, self).__init__()
		self.affine1 = nn.Linear(6400, 200).cuda()
		self.affine2 = nn.Linear(200, 3).cuda()

		self.savedLogProbs = []
		self.rewards = []

	def forward(self, x):
		x = x.cuda()
		x = F.relu(self.affine1(x))
		return F.softmax(self.affine2(x), dim = 1)

def select_action(policy, state):
	state = torch.from_numpy(state).float().unsqueeze(0)
	state.requires_grad = True
	probs = policy(state)
	m = Categorical(probs) # ?
	action = m.sample()
	policy.savedLogProbs.append(m.log_prob(action))
	return action.data[0]

def update_policy(policy, optimizer):
	R = 0
	rewards = []
	loss = []
	for r in policy.rewards[::-1]:
		R = r + 0.99 * R
		rewards.append(R)
	rewards = torch.Tensor(rewards[::-1]).cuda()
	rewards = (rewards - rewards.mean()) / rewards.std() #+ np.finfo(np.float32).eps
	
	#loss = rewards * torch.Tensor(policy.savedLogProbs).cuda()
	for log_prob, reward in zip(policy.savedLogProbs, rewards):
		loss.append(-log_prob * reward)

	optimizer.zero_grad()
	loss = torch.cat(loss).sum().cuda()
	loss.backward()
	optimizer.step()

	del policy.rewards[:]
	del policy.savedLogProbs[:]

	return loss

# built policy network
policyNet = PolicyNet().cuda()

# construct a optimal function
optimizer = optim.RMSprop(policyNet.parameters(), lr=1e-3, weight_decay=0.99)

# check & load pretrain model
if os.path.isfile('pg_params.pkl'):
    print('Load Policy Network parametets ...')
    policyNet.load_state_dict(torch.load('pg_params.pkl'))



# main
env = gym.make("Pong-v0")
runningReward = 0 
rewardSum = 0
win = 0
lose = 0
preState = np.zeros(6400)
for i in count(1):
	observation = env.reset()
	while True:
		env.render()
		curState = preprocess(observation)
		state = curState - preState
		preState = curState
		action = select_action(policyNet, state) + 1
		observation, reward, done, info = env.step(action)
		rewardSum += reward
		if (reward == 1): win += 1
		if (reward == -1): lose += 1
		policyNet.rewards.append(reward)
		if done:		
			break

	runningReward = runningReward * 0.99 + rewardSum * 0.01
	print('resetting env. episode result was (win)%d vs (lose)%d. reward mean: %f' % (win, lose, runningReward))
	rewardSum = 0
	win = 0
	lose = 0
'''	
	if i % 1 == 0:
		loss = update_policy(policyNet, optimizer)
		print('ep %d: policy network parameters updated' % (i))

	if i % 50 == 0:
		print('ep %d: model saving...' % (i))
		torch.save(policyNet.state_dict(), 'pg_params.pkl')
	
'''





